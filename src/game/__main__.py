from argparse import ArgumentParser
from src.game import game


parser = ArgumentParser(description="Play the game of 2048")
parser.add_argument("--difficulty", "-d",
                    type=str,
                    default="normal",
                    choices=["easy", "normal", "hard", "evil"],
                    help="the difficulty of the game")
parser.add_argument("--skin", "-s",
                    type=str,
                    default="default",
                    choices=["custom", "default", "terminal", "base"],
                    help="the colorscheme used in the output, if 'terminal' the show the output in the terminal, else start gui in a new window")
parser.add_argument("--field-size", "-f",
                    dest="size",
                    type=int,
                    default=4,
                    help="the size of the game field")
args = parser.parse_args()

game(difficulty=args.difficulty, skin=args.skin, field_size=(args.size,) * 2).loop()