import os
import keras


# disable compatabilty warning output spam
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
keras.backend.tf.compat.v1.logging.set_verbosity("ERROR")


DATA_FORMAT = "channels_last"
NUM_CHANNELS = 20
MINIBATCH_SIZE = 64
MEMORY_SIZE = 4096
TARGET_UPDATE = 8


from src.neuralnet.tensorboard import ModifiedTensorBoard
from src.neuralnet.agent import Agent
