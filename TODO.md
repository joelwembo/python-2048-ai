# todo

## game

* function that yields all possible tile objects with probability
* animations:
  * steps:
      (1) in slide:
	  (1.1) backup tile states
	  (1.2) slide them
	  (1.3) call animate or so (taking 3 args: direction, from, to; where to may be let out and replaced by the current tiles)
	  (2) animate (similar to draw but in a blocking loop):
	  (2.0) first draw all tiles as empty, then non empty ones (split like in add_tile)
	  (2.1) repeat this 20 - 30 times:
	  (2.1.0) get the position differences as to - from
	  (2.1.1) iterate over each tile
	  (2.1.2) draw it to new position
* evil mode (like https://aj-r.github.io/Evil-2048/)

## algorithmic bot

* highest-in-corner startegy

## neuralnet

* policy network
* no conv neural net
* possible norminalizations:
  * nothing, just feed it the values
  * f(x) = 1 - 0.5 ** x
  * one hot encoding

### reward

* punish for no progress (this time for real)
* reward for highest tile improvements
* punish for having to tiles with the same value not adjacent to each other (higher tile value => higher punish)
