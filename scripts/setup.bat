@echo off

:: update repo
git pull

:: create venv if it doesnt exist
if not exist venv py -3 -m venv venv

:: activate it
source venv/scripts/activate.bat

:: install all packages
pip install -r reqirements.txt